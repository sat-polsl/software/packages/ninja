function(_ninja_version)
    execute_process(
        COMMAND ${NINJA_EXECUTABLE} --version
        OUTPUT_VARIABLE full_ninja_version
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    message(TRACE "Ninja version full: ${full_ninja_version}")
    
    string(REGEX MATCH "^[0-9]+\.[0-9]+\.[0-9]+" ninja_version "${full_ninja_version}")

    message(DEBUG "Ninja version: ${ninja_version}")
    set(NINJA_VERSION ${ninja_version} PARENT_SCOPE)
endfunction()

function(find_ninja VERSION)
    if(NINJA_FOUND)
        return()
    endif()

    set(OPTIONS REQUIRED NO_SYSTEM_PATH)
    set(ONE_VALUE)
    set(MULTI_VALUE)
    cmake_parse_arguments(PARSE_ARGV 1 FIND_NINJA "${OPTIONS}" "${ONE_VALUE}" "${MULTI_VALUE}")

    if (${CMAKE_HOST_SYSTEM_NAME} STREQUAL Windows)
        set(EXE ".exe")
    endif ()

    if(FIND_NINJA_REQUIRED)
        set(REQUIRED "REQUIRED")
    endif()
    if(FIND_NINJA_NO_SYSTEM_PATH)
        set(NO_SYSTEM_PATH "NO_SYSTEM_ENVIRONMENT_PATH")
    endif()

    find_program(NINJA_EXECUTABLE NAMES ninja${EXE} PATHS ${Ninja_ROOT} ENV{Ninja_ROOT} ${REQUIRED} ${NO_SYSTEM_PATH})

    if(NINJA_EXECUTABLE)
        _ninja_version()

        if(NOT VERSION VERSION_EQUAL NINJA_VERSION)
            if(NOT FIND_NINJA_REQUIRED)
                message(VERBOSE "Found Ninja: ${Ninja} (found version \"${NINJA_VERSION}\", required \"${VERSION}\")")
                unset(NINJA_EXECUTABLE CACHE)
                set(NINJA_FOUND FALSE PARENT_SCOPE)
                return()
            endif()
            message(FATAL_ERROR "Ninja not found. (Required: ${VERSION}, found: ${NINJA_VERSION})")
        endif()

        message(STATUS "Found Ninja: ${NINJA_EXECUTABLE} (found version \"${NINJA_VERSION}\", required \"${VERSION}\")")
        set(NINJA_FOUND TRUE PARENT_SCOPE)
    else()
        message(DEBUG "Ninja not found")
        set(NINJA_FOUND FALSE PARENT_SCOPE)
    endif()
endfunction()
