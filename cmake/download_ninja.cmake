set(NINJA_WINDOWS_URL https://github.com/ninja-build/ninja/releases/download/v${VERSION}/ninja-win.zip)
set(NINJA_LINUX_URL https://github.com/ninja-build/ninja/releases/download/v${VERSION}/ninja-linux.zip)

macro(_download_for_linux)
    message(VERBOSE "Downloading Ninja for Linux from: ${NINJA_LINUX_URL}")

    file(DOWNLOAD ${NINJA_LINUX_URL} ${ROOT}/ninja.zip)
    file(ARCHIVE_EXTRACT 
         INPUT ${ROOT}/ninja.zip
         DESTINATION ${ROOT})
    file(REMOVE ${ROOT}/ninja.zip)
endmacro()

macro(_download_for_windows)
    message(VERBOSE "Downloading Ninja for Windows from: ${NINJA_WINDOWS_URL}")

    file(DOWNLOAD ${NINJA_WINDOWS_URL} ${ROOT}/ninja.zip)
    file(ARCHIVE_EXTRACT 
         INPUT ${ROOT}/ninja.zip
         DESTINATION ${ROOT})
    file(REMOVE ${ROOT}/ninja.zip)
endmacro()

function(_calculate_hash RESULT)
    message(DEBUG "Calculating hashes")
    
    file(GLOB FILES "${ROOT}/*")

    set(HASH_LIST "")
    foreach(FILE ${FILES})
        set(HASH_VALUE "")
        file(MD5 ${FILE} HASH_VALUE)
        list(APPEND HASH_LIST ${HASH_VALUE})

        message(DEBUG "Hashed ${FILE}: ${HASH_VALUE}")
    endforeach()

    message(DEBUG "Hashing result: ${HASH_LIST}")
    set(${RESULT} ${HASH_LIST} PARENT_SCOPE)
endfunction()

macro(download_ninja)
    if(NOT IS_DIRECTORY ${ROOT})
        file(MAKE_DIRECTORY ${ROOT})
    endif()

    if(WIN32)
        _download_for_windows()
    elseif(UNIX)
        _download_for_linux()
    endif()

    set(HASH "")
    _calculate_hash(HASH)

    file(WRITE ${ROOT}/.md5 "${HASH}")
endmacro()

